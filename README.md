# Вариант 6: Buddhism, Botany, Transport

**Результат выполнения программы:**

График elbow метода:

![Image alt](https://gitlab.com/Lobay/oirs_lr4_5/raw/master/graphs/3.png)

Оптимальное количество кластеров 5.

**Визуализации PCA и TSNE исходных данных:**

![Image alt](https://gitlab.com/Lobay/oirs_lr4_5/raw/master/graphs/1.png) 
![Image alt](https://gitlab.com/Lobay/oirs_lr4_5/raw/master/graphs/2.png)

**Разбивка массива признаков на кластеры с помощью алгоритмов KMeans, MiniBatchKMeans, DBSCAN и визуализация предсказанных и истинных меток**

    KMeans clustering
                                 title  cluster
    0                         Buddhism        0
    14                        Mahayana        0
    12               Refuge (Buddhism)        0
    11              Rebirth (Buddhism)        0
    10                Chinese Buddhism        0
    9                Nichiren Buddhism        0
    8        Buddhism and Christianity        0
    13                       Vajrayana        0
    6              Buddhism by country        0
    5              History of Buddhism        0
    4               Saṃsāra (Buddhism)        0
    3               Nirvana (Buddhism)        0
    2                 Tibetan Buddhism        0
    1                Buddhism in Japan        0
    7              Schools of Buddhism        0
    32                Public transport        1
    33                Active transport        1
    34               Freight transport        1
    35  HTTP Strict Transport Security        1
    36        Transport Layer Security        1
    40              Transport in India        1
    38        Rail transport in Russia        1
    39            Supersonic transport        1
    41             Transport for Wales        1
    42              Pipeline transport        1
    31                  Rail transport        1
    37                 Transport layer        1
    30                       Transport        1
    22             Receptacle (botany)        1
    27            Aestivation (botany)        1
    26                 Stigma (botany)        1
    24                 Capsule (fruit)        1
    23                    Awn (botany)        1
    43                  Land transport        1
    20              Branches of botany        1
    19                   Bark (botany)        1
    18                  Berry (botany)        1
    17     Glossary of botanical terms        1
    15                          Botany        1
    44                       Logistics        1
    29               Peduncle (botany)        2
    25                      Botany 500        2
    21                     Botany Boyz        2
    16                      Botany Bay        2
    28              Botany Bay, London        2

![Image alt](https://gitlab.com/Lobay/oirs_lr4_5/raw/master/graphs/4.png) 
![Image alt](https://gitlab.com/Lobay/oirs_lr4_5/raw/master/graphs/5.png) 

    MiniBatchKMeans clustering:
                                 title  cluster
    36        Transport Layer Security        0
    0                         Buddhism        1
    32                Public transport        1
    30                       Transport        1
    37                 Transport layer        1
    28              Botany Bay, London        1
    38        Rail transport in Russia        1
    39            Supersonic transport        1
    40              Transport in India        1
    24                 Capsule (fruit)        1
    23                    Awn (botany)        1
    43                  Land transport        1
    42              Pipeline transport        1
    19                   Bark (botany)        1
    18                  Berry (botany)        1
    33                Active transport        1
    17     Glossary of botanical terms        1
    15                          Botany        1
    1                Buddhism in Japan        1
    2                 Tibetan Buddhism        1
    3               Nirvana (Buddhism)        1
    4               Saṃsāra (Buddhism)        1
    5              History of Buddhism        1
    6              Buddhism by country        1
    7              Schools of Buddhism        1
    8        Buddhism and Christianity        1
    9                Nichiren Buddhism        1
    10                Chinese Buddhism        1
    11              Rebirth (Buddhism)        1
    12               Refuge (Buddhism)        1
    13                       Vajrayana        1
    14                        Mahayana        1
    16                      Botany Bay        1
    34               Freight transport        1
    41             Transport for Wales        2
    44                       Logistics        3
    31                  Rail transport        3
    29               Peduncle (botany)        3
    27            Aestivation (botany)        3
    26                 Stigma (botany)        3
    25                      Botany 500        3
    21                     Botany Boyz        3
    20              Branches of botany        3
    35  HTTP Strict Transport Security        3
    22             Receptacle (botany)        4

![Image alt](https://gitlab.com/Lobay/oirs_lr4_5/raw/master/graphs/6.png) 
![Image alt](https://gitlab.com/Lobay/oirs_lr4_5/raw/master/graphs/7.png) 

    DBSCAN clustering:
                                 title  cluster
    22             Receptacle (botany)       -1
    24                 Capsule (fruit)       -1
    25                      Botany 500       -1
    26                 Stigma (botany)       -1
    27            Aestivation (botany)       -1
    28              Botany Bay, London       -1
    29               Peduncle (botany)       -1
    31                  Rail transport       -1
    32                Public transport       -1
    23                    Awn (botany)       -1
    33                Active transport       -1
    35  HTTP Strict Transport Security       -1
    36        Transport Layer Security       -1
    37                 Transport layer       -1
    38        Rail transport in Russia       -1
    39            Supersonic transport       -1
    40              Transport in India       -1
    41             Transport for Wales       -1
    42              Pipeline transport       -1
    34               Freight transport       -1
    21                     Botany Boyz       -1
    44                       Logistics       -1
    3               Nirvana (Buddhism)       -1
    7              Schools of Buddhism       -1
    8        Buddhism and Christianity       -1
    9                Nichiren Buddhism       -1
    20              Branches of botany       -1
    11              Rebirth (Buddhism)       -1
    12               Refuge (Buddhism)       -1
    6              Buddhism by country       -1
    4               Saṃsāra (Buddhism)       -1
    1                Buddhism in Japan       -1
    15                          Botany       -1
    16                      Botany Bay       -1
    17     Glossary of botanical terms       -1
    18                  Berry (botany)       -1
    19                   Bark (botany)       -1
    2                 Tibetan Buddhism        0
    10                Chinese Buddhism        0
    0                         Buddhism        0
    13                       Vajrayana        0
    14                        Mahayana        0
    5              History of Buddhism        0
    30                       Transport        1
    43                  Land transport        1

![Image alt](https://gitlab.com/Lobay/oirs_lr4_5/raw/master/graphs/8.png) 
![Image alt](https://gitlab.com/Lobay/oirs_lr4_5/raw/master/graphs/9.png) 

**Дендограмма кластеризации:**

![Image alt](https://gitlab.com/Lobay/oirs_lr4_5/raw/master/graphs/10.png) 






























