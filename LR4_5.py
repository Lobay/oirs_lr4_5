import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans, MiniBatchKMeans, DBSCAN
from scipy.cluster.hierarchy import dendrogram, ward
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
import wikipedia

# минимальное количество статей по каждой теме
N = 15
articles = ['Buddhism', 'Botany', 'Transport']

# функция для скачивания статей по каждой теме
def download_articles(themes=articles):
    # Массив статей одной темы
    article_list = list()
    # Массив всех тем
    article_names = list()
    articles_themes = []
    # Прогон всех статей
    for i in themes:
        count = 0
        # Прогон всех статей одной темы
        for article in wikipedia.search(i, results=2*N):
            if count >= N:
                break
            try:
                article_list.append(wikipedia.page(article).content)
            except (wikipedia.exceptions.PageError,
                    wikipedia.exceptions.DisambiguationError):
                continue
            print(f"Downloaded article ({i}): " + article)
            article_names.append(article)
            articles_themes.append(i)
            count += 1
    return article_list, article_names, articles_themes

# Конвертируем содержимое в матрицу TF-IDF фичей
def matrix_tf():
    data, name, themes = download_articles()
    vectorize = TfidfVectorizer(stop_words={'english'})
    vector_new = vectorize.fit_transform(data)
    return vector_new, name, themes

# Строим график elbow-метода, чтобы увидеть оптимальное k.
def plot_elbow_method(data):
    squared_error = list()
    k_range = range(2, 10)
    for k in k_range:
        model = KMeans(n_clusters=k, max_iter=200, n_init=10).fit(data)
        squared_error.append(model.inertia_)
    # Создание и вывод графика
    plt.plot(k_range, squared_error, 'bx-')
    plt.xlabel('k')
    plt.ylabel('Sum of squared distances')
    plt.title('Elbow Method For Optimal k')
    plt.show()

# Функция для рисования точек после PCA или TSNE
def plot_2d_points(points, labels, plot_title):
    fig = plt.figure()
    wiki = pd.DataFrame(
        list(zip(points[:, 0], points[:, 1], labels)),
        columns=['x', 'y', 'cluster']
    )
    for cluster in np.unique(wiki.cluster):
        subset = wiki[wiki.cluster == cluster]
        plt.scatter(subset.x, subset.y, label=cluster)
    plt.legend()
    plt.title(plot_title)
    plt.show()


# Визуализация предсказанных и истинных меток
def plot_2d_points_predicted(points, labels_true, labels_pred, plot_title):
    # Будем рисовать 2 графика
    fig, axes = plt.subplots(1, 2, figsize=(10, 6))
    # Создадим датафрейм с истинными кластерами
    wiki_true = pd.DataFrame(
        list(zip(points[:, 0], points[:, 1], labels_true)),
        columns=['x', 'y', 'cluster']
    )
    # Для каждого истинного кластера нарисуем точки с соотв. меткой
    for cluster in np.unique(wiki_true.cluster):
        subset = wiki_true[wiki_true.cluster == cluster]
        axes[0].scatter(subset.x, subset.y, label=cluster)
        axes[0].set_title('True')
        axes[0].legend()

    # Создадим датафрейм с предсказанными кластерами
    wiki_pred = pd.DataFrame(
        list(zip(points[:, 0], points[:, 1], labels_pred)),
        columns=['x', 'y', 'cluster']
    )
    # Для каждого предсказанного кластера рисуем точки с соответствующей меткой
    for cluster in np.unique(wiki_pred.cluster):
        subset = wiki_pred[wiki_pred.cluster == cluster]
        axes[1].scatter(subset.x, subset.y, label=cluster)
        axes[1].set_title('Predicted')
        axes[1].legend()

    fig.suptitle(plot_title)
    plt.show()


# Функция для уменьшения размерностей PCA и TSNE
def reduce_dimension(data):
    pca = PCA(n_components=2)
    tsne = TSNE(n_components=2)
    pca_data = pca.fit_transform(data.todense())
    tsne_data = tsne.fit_transform(data)
    return pca_data, tsne_data


# Визуализации PCA и TSNE исходных данных
def show_scatters(data, true_labels):
    pca, tsne = reduce_dimension(data)
    plot_2d_points(pca, true_labels, 'PCA of source data')
    plot_2d_points(tsne, true_labels, 'TSNE of source data')


# Кластеризация k-средних
def kmeans(data, names, themes):
    k = 3
    model = KMeans(n_clusters=k, init='k-means++', max_iter=200, n_init=10).fit(data)
    labels = model.labels_
    wiki = pd.DataFrame(list(zip(names, labels)), columns=['title', 'cluster'])
    print('KMeans clustering')
    print(wiki.sort_values(by=['cluster']))

    pca, tsne = reduce_dimension(data)
    plot_2d_points_predicted(pca, themes, labels, 'KMeans (PCA)')
    plot_2d_points_predicted(tsne, themes, labels, 'KMeans (TSNE)')


# Кластеризация MiniBatchKMeans
def minibatch(data, names, themes):
    k = 5
    model = MiniBatchKMeans(n_clusters=k, batch_size=6, max_iter=200, n_init=10).fit(data)
    labels = model.labels_
    wiki = pd.DataFrame(list(zip(names, labels)), columns=['title', 'cluster'])
    print('MiniBatchKMeans clustering:')
    print(wiki.sort_values(by=['cluster']))

    pca, tsne = reduce_dimension(data)
    plot_2d_points_predicted(pca, themes, labels, 'MiniBatch (PCA)')
    plot_2d_points_predicted(tsne, themes, labels, 'MiniBatch (TSNE)')


# Кластеризация DBSCAN из векторного массива
def dbscan(data, names, themes):
    model = DBSCAN(eps=0.5, min_samples=2).fit(data)
    labels = model.labels_
    wiki = pd.DataFrame(list(zip(names, labels)), columns=['title', 'cluster'])
    print('DBSCAN clustering:')
    print(wiki.sort_values(by=['cluster']))

    pca, tsne = reduce_dimension(data)
    plot_2d_points_predicted(pca, themes, labels, 'DBSCAN (PCA)')
    plot_2d_points_predicted(tsne, themes, labels, 'DBSCAN (TSNE)')


# Иерархическая кластеризация
def hierarchy(data, names):
    # Находим расстояние между статьями
    # cosine_similarity - коэффициент схожести
    distance = 1 - cosine_similarity(data)
    matrix = ward(distance)
    # Построение и вывод дендограммы
    plt.figure(figsize=(50, 50))
    plt.title('Hierarchical Clustering Dendrogram')
    plt.xlabel('Sample index')
    plt.ylabel('Distance')
    dendrogram(matrix, orientation="right", labels=names)
    plt.show()


if __name__ == '__main__':
    val1, val2, val3 = matrix_tf()
    show_scatters(val1, val3)
    plot_elbow_method(val1)
    kmeans(val1, val2, val3)
    minibatch(val1, val2, val3)
    dbscan(val1, val2, val3)
    hierarchy(val1, val2)
